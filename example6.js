const EventEmitter = require('events');

const myEmitter = new EventEmitter();

const myCallback_1 = (event) => {
  console.log(`😬 I will open the gift for the next listener:`, event.gift);
  event.gift = '🚲';
}

const myCallback_2 = (event) => {
  console.log(`🤩 I received a bicycle!!!:`, event.gift);
}

myEmitter.on('my event 🎉', myCallback_1);
myEmitter.on('my event 🎉', myCallback_2);

myEmitter.emit('my event 🎉', { gift: '🎁'} );
