const EventEmitter = require('events');

const myEmitter = new EventEmitter();

const myCallback_1 = () => {
  console.log(`🤬 I don't have food`);
}

const myCallback_2 = (food) => {
  console.log(`😊 I have a ${food}!!!`);
}

const myCallback_3 = (food1, food2) => {
  console.log(`😋 I have a ${food1} and a ${food2}!!!`);
}

const myCallback_4 = (...food) => {
  const foods = food.join(', ')
  console.log(`😎 I have for eat: ${foods}!!!`);
}

myEmitter.on('my event 🎉', myCallback_1);
myEmitter.on('my event 🎉', myCallback_2);
myEmitter.on('my event 🎉', myCallback_3);
myEmitter.on('my event 🎉', myCallback_4);

myEmitter.emit('my event 🎉', '🍔', '🍕', '🥓');
