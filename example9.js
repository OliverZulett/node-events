const EventEmitter = require('events');

const myEmitter = new EventEmitter();

const myCallback = () => {
  console.log(`Listener: I'm going crazy 😵`);
  myEmitter.removeListener("my repetitive event ⏰", myCallback);
}

myEmitter.on('my repetitive event ⏰', myCallback);

setInterval(() => {
  console.log(`I'm gonna emit something ...`);
  myEmitter.emit("my repetitive event ⏰");
  //we can also unsubscribe after a emit
  // myEmitter.removeListener("my repetitive event ⏰", myCallback);
}, 1000);
