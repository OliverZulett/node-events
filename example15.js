const EventEmitter = require("events");

class myCustomEventEmitter extends EventEmitter {}

const myEmitter = new myCustomEventEmitter();

myEmitter.on("Custom event ✨", () =>
  console.log(`🎃 I'm a cool custom event`)
);

myEmitter.emit("Custom event ✨");
