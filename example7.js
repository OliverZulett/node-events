const EventEmitter = require('events');

const myEmitter = new EventEmitter();

const myCallback_1 = (event) => 
  event.print_from_where_im_executed(`Listener ☝`);

const myCallback_2 = (event) => 
  event.print_from_where_im_executed(`Listener ✌`);

myEmitter.on('my event 🎉', myCallback_1);
myEmitter.on('my event 🎉', myCallback_2);

myEmitter.emit("my event 🎉", {
  print_from_where_im_executed: (listener) =>
    console.log(`🎆 I'm a cool funcion executed from: `, listener),
});
