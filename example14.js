const EventEmitter = require('events');

const myEmitter = new EventEmitter();

// one way to handle the error
myEmitter.on('error', (err) => console.error(`🤬 Did u broke something ?? \n${err.message}`));

myEmitter.emit('error', new Error('Something is broken 😩'));

// another way to handle the error// one way to handle the error
// try {
//   myEmitter.emit('error', new Error('Something is broken 😩'));
// } catch (error) {
//   console.error(error.message)
// }
