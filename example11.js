const EventEmitter = require('events');

const myEmitter = new EventEmitter();

const myCallback_1 = () => {
  console.log(`🤪 I'm the first callback!!!`);
}
const myCallback_2 = () => {
  console.log(`😎 I'm the second callback!!!`);
}

myEmitter.on('my event 🎉', myCallback_2);
myEmitter.on('my event 🎉', myCallback_1);

myEmitter.emit('my event 🎉');

console.log('number of listeners: ',myEmitter.listenerCount('my event 🎉'));
console.log('Listeners: ',myEmitter.listeners('my event 🎉'));
