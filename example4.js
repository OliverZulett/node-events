const EventEmitter = require('events');

const myEmitter = new EventEmitter();

const myCallback_1 = () => {
  console.log(`🤬 Now I'm the second callback!!!`);
}
const myCallback_2 = () => {
  console.log(`😁 Now I'm the first callback!!!`);
}

// This is a nice consequence of the
// single-threaded nature of Node.js 🤙
myEmitter.on('my event 🎉', myCallback_2);
myEmitter.on('my event 🎉', myCallback_1);

myEmitter.emit('my event 🎉');
