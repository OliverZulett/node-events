const EventEmitter = require('events');

let listenersCalled = 0;
const myEmitter = new EventEmitter();

function call_listener() {
  myEmitter.on('my event 🎉', () => {
    console.log(`😀 I'm listener number: `,listenersCalled++);
  })
}

for (var i = 0; i < 11; i++) call_listener();

myEmitter.emit('my event 🎉');

console.log('max num of listeners: ', myEmitter.getMaxListeners());
console.log('listeners called: ', listenersCalled);
console.log('listeners number: ', myEmitter.listenerCount('my event 🎉'));