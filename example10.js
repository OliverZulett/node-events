const EventEmitter = require('events');

const myEmitter = new EventEmitter();

const myCallback = () => {
  console.log(`Listener: I'm only be executed once 😎`);
}

myEmitter.once('my repetitive event ⏰', myCallback);

setInterval(() => {
  console.log(`I'm gonna emit something ...`);
  myEmitter.emit("my repetitive event ⏰");
}, 1000);
