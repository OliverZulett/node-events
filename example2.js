import EventEmitter from 'events';

const myEmitter = new EventEmitter();

const myCallback = () => {
  console.log(`😎 I'm an awesome callback!!!`);
}

myEmitter.emit('my event 🎉');

myEmitter.on('my event 🎉', myCallback);
