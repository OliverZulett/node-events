const EventEmitter = require('events');

const myEmitter = new EventEmitter();

const myCallback = () => {
  console.log(`😎 I'm an awesome callback!!!`);
}

myEmitter.on('my event 🎉', myCallback);

// ---> event emitter always goes after listeners
myEmitter.emit('my event 🎉');
