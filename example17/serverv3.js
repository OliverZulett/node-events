// Server uses EventEmitter library
const server = require('net').createServer();
// identifier
let counter = 0;
// for storage each connection
let sockets = [];

function timestamp() {
  const now = new Date();
  return `${now.getHours()}:${now.getMinutes()}`;
}

server.on('connection', socket => {
  //assign identifier
  socket.id = counter++;

  console.log(`Client: ${socket.id} connected`);
  // send welcome message
  socket.write(`Please enter your Nick name!\n`);
  // socket are an eventEmitter to
  socket.on('data', dataReceived => {
    // We've validate if a client is not register
    if (!sockets[socket.id]) {
      socket.name = dataReceived.toString().trim();
      socket.write(`Welcome ${socket.name}!\n`);
      //storage each socket connection
      sockets[socket.id] = socket;
      // avoid sends this message to all clients
      return;
    }
    console.log(`Client: ${socket.id} writes: ${dataReceived}`);
    // loop for each socket to send data
    Object.entries(sockets).forEach(([key, clientSocket]) => {
      // avoid return the same data to client
      if (key == socket.id) return;
      // send data received now with name instead of id client and the timestamp
      clientSocket.write(`${socket.name} ${timestamp()}: ${dataReceived}`);
    })
  });
  // finish the connection
  socket.on('end', () => {
    console.log(`Client: ${socket.id} ends his session`);
    // removes socket when client goes off
    delete sockets[socket.id];
  });
})

server.listen(8000, () => console.log('Server bound'));
