// Server uses EventEmitter library
const server = require('net').createServer();

server.on('connection', socket => {

  console.log('Client connected');
  // send welcome message
  socket.write('Welcome new client!\n');
  // socket are an eventEmitter to
  socket.on('data', dataReceived => {
    console.log(`I've received: `, dataReceived);
    // send data received
    socket.write(`You've send me: ${dataReceived}`);
  });
  // finish the connection
  socket.on('end', () => {
    console.log('Client disconnected');
  });
})

server.listen(8000, () => console.log('Server bound'));
