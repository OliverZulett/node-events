// Server uses EventEmitter library
const server = require('net').createServer();
// identifier
let counter = 0;
// for storage each connection
let sockets = [];

server.on('connection', socket => {
  //assign identifier
  socket.id = counter++;
  //storage each socket connection
  sockets[socket.id] = socket;

  console.log(`Client: ${socket.id} connected`);
  // send welcome message
  socket.write(`Welcome ${socket.id}!\n`);
  // socket are an eventEmitter to
  socket.on('data', dataReceived => {
    console.log(`Client: ${socket.id} writes: ${dataReceived}`);
    // loop for each socket to send data
    Object.entries(sockets).forEach(([key, clientSocket]) => {
      // send data received
      clientSocket.write(`${socket.id}: ${dataReceived}`);
    })
  });
  // finish the connection
  socket.on('end', () => {
    console.log(`Client: ${socket.id} ends his session`);
    // removes socket when client goes off
    delete sockets[socket.id];
  });
})

server.listen(8000, () => console.log('Server bound'));
