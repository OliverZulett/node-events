const EventEmitter = require('events');

const myEmitter = new EventEmitter();

const myCallback = () => 
  console.log(`Listener: I'm going crazy 😵`);

// This listener will listen 4ever
myEmitter.on('my repetitive event ⏰', myCallback);

setInterval(() => {
  console.log(`EventEmitter: I'm going emit something ...`);
  myEmitter.emit("my repetitive event ⏰");
}, 1000);
