const TicketManager = require("./ticketManager");
const EmailService = require("./emailService");
const DatabaseService = require("./databaseService");

const ticketManager = new TicketManager(3);
const emailService = new EmailService();
const databaseService = new DatabaseService();

const onBuy = () => {
  console.log("I will be removed soon");
};
const noBuy = () => {
  console.log("Make it nothing");
};

ticketManager.on('buy 🤑', (email, price, timestamp) => {
  console.log(`Someone buys a Ticket 🎉`);
  emailService.send(email);
  databaseService.save(email, price, timestamp);
});
ticketManager.on('error', (err) => console.log(`There are some error 😱: \n${err.message}`));
ticketManager.on('buy 🤑', onBuy);

console.log(`We have ${ticketManager.listenerCount('buy 🤑')} listener(s) for the buy event`);
console.log(`We have ${ticketManager.listenerCount("error")} listener(s) for the error event`);
console.log(`We added a new event listener bringing our total count for the buy event to: ${ticketManager.listenerCount('buy 🤑')}`);
ticketManager.buy("test@email.com", 20);

ticketManager.off('buy 🤑', noBuy); // removes the last listener according to callback
console.log(`We added a new event listener bringing our total count for the buy event to: ${ticketManager.listenerCount('buy 🤑')}`);
ticketManager.buy("test@email.com", 20);

ticketManager.removeAllListeners('buy 🤑');
console.log(`We have ${ticketManager.listenerCount('buy 🤑')} listeners for the buy event`);
ticketManager.buy("test@email", 20);
console.log("The last ticket was bought");

ticketManager.buy("test@email", 20);
console.log(`We have ${ticketManager.listenerCount('buy 🤑')} listener(s) for the buy event`);
console.log(`We have ${ticketManager.listenerCount("error")} listener(s) for the error event`);
