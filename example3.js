const EventEmitter = require('events');

const myEmitter = new EventEmitter();

const myCallback_1 = () => {
  console.log(`😎 I'm the first awesome callback!!!`);
}
const myCallback_2 = () => {
  console.log(`😒 I'm the second callback!!!`);
}

// order matters when u register listeners
myEmitter.on('my event 🎉', myCallback_1);
myEmitter.on('my event 🎉', myCallback_2);

myEmitter.emit('my event 🎉');
