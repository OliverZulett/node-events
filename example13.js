const EventEmitter = require('events');

let listenersCalled = 0;
const myEmitter = new EventEmitter();

//---> 0 dont have limits
myEmitter.setMaxListeners(20);

function call_listener() {
  myEmitter.on('my event 🎉', () => {
    console.log(`😀 I'm listener number: `,listenersCalled++);
  });
}

for (var i = 0; i < 50; i++) call_listener();

myEmitter.emit('my event 🎉');

console.log('max num of listeners: ', myEmitter.getMaxListeners());
console.log('listeners called: ', listenersCalled);
console.log('listeners number: ', myEmitter.listenerCount('my event 🎉'));