const EventEmitter = require('events');

const myEmitter = new EventEmitter();

const myCallback_1 = (event) => {
  console.log(`🤩 I received a gift!!!:`, event.gift);
}

myEmitter.on('my event 🎉', myCallback_1);

myEmitter.emit('my event 🎉', { gift: '🎁'} );
